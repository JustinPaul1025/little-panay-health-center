<nav class="topnav navbar" role="navigation">
    <div class="navbar-brand">
        <a class="is-paddingless p-t-8" href="{{ url('/home') }}">
            <img src="{{ asset('img/logo.png') }}" width="80" height="80">
        </a>
    </div>

    <div id="navbarBasicExample" class="navbar-menu has-background-info">
        <div class="navbar-end">
            <a class="navbar-item has-text-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
                <span><i class="fas fa-sign-out-alt has-text-danger"></i></span> {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>
</nav>