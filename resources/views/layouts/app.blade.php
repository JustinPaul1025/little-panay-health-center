<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('layouts.head')
<body>
    <section class="hero is-bold">
        <div id="app">
            <div>
                @include('layouts.topnav') 
            </div>
            <div class="columns m-r-10 m-l-10">
                <div class="column is-3">
                    <div id="sidebar">
                        @include('layouts.sidecard')
                    </div>
                </div>
                <div class="column">
                    <div id="main-content">
                        @section('content')
                        <router-view></router-view>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
    </section>
</body>
</html>
