<nav class="panel">
    <p class="panel-heading">
        <i class="fas fa-user-circle is-size-6 has-text-white "></i>&nbsp {{ Auth::user()->last_name }}, {{ Auth::user()->first_name }}
    </p>
    <router-link to="/" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-chart-line"></i>
      </span>
      Dashboard
    </router-link>
    <router-link to="/child" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-baby"></i>
      </span>
      Child Health Card
    </router-link>
    <router-link to="/maternal" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-female"></i>
      </span>
      Maternal Health Record
    </router-link>
    <router-link to="/individual" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-file-medical"></i>
      </span>
      Individual Treatment Record
    </router-link>
    <router-link to="/familyPlanning" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-users"></i>
      </span>
      Family Planning
    </router-link>
    <router-link to="/familyProfile" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-copy"></i>
      </span>
      Family Profile
    </router-link>
    @if(Auth::user()->role == 'admin')
      <router-link to="/users" class="panel-block">
        <span class="panel-icon">
          <i class="fas fa-users"></i>
        </span>
        Users
      </router-link>
    @endif  
    <router-link to="/userSettings" class="panel-block">
      <span class="panel-icon">
        <i class="fas fa-cogs"></i>
      </span>
      Settings
    </router-link>
  </nav>