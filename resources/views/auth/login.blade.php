<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name')}}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="has-havbar-fixed-top">
    <section class="hero is-fullheight is-dark is-bold">
        <div class="hero-body" id="app">
            <div class="container" id="login">
                <div class="columns is-vcentered is-centered">
                    <div class="column is-8">
                        <div class="columns">
                            <div class="column is-7 has-background-primary has-text-centered logo">
                                <img src="{{ asset('img/logo.png') }}" width="100%">
                                <p class="has-text-weight-bold is-size-6">BARANGAY LITTLE PANAY HEALTH CENTER INFORMATION SYSTEM with SMS NOTIFICATION</p>
                            </div>
                            <div class="column form is-paddingless">
                                <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                <div class="box">
                                    <label class="label has-text-primary is-size-5">Username</label>
                                    <p class="control has-icon has-icon-right">
                                        <input id="username" type="text" class="input @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="username">

                                        @error('username')
                                            <span class="invalid-feedback has-text-white" role="alert">
                                                <strong class="has-text-danger">{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <span class="icon is-small">
                                            <i class="fa fa-warning"></i>
                                        </span>
                                    </p>
                                    <label class="label has-text-primary is-size-5">Password</label>
                                    <p class="control has-icon has-icon-right">
                                        <input type="password" placeholder="●●●●●●●" class="input @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong class="has-text-danger">{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <span class="icon is-small">
                                            <i class="fa fa-warning"></i>
                                        </span>
                                    </p><br>
                                    <p class="control is-pulled-right">
                                        <button type="submit" class="button is-primary has-text-weight-bold">
                                            {{ __('Login') }}
                                        </button>
                                    </p>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </section>
</body>
</html>