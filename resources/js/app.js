

require('./bootstrap');

window.Vue = require('vue');

import VuejsPaginate from 'vuejs-paginate'
Vue.component('paginate', VuejsPaginate)

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Buefy from 'buefy'
Vue.use(Buefy);

import Swal from 'sweetalert2';
window.Swal = Swal;
const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
window.toast = toast;

const routes = [
    { path: '/', component: require('./components/dashboard.vue').default },

    { path: '/child', component: require('./components/child/index.vue').default },
    { path: '/childHealthCard/form/:id', component: require('./components/child/childRegistrationForm.vue').default },
    { path: '/childHealthCard/manage/:id', component: require('./components/child/manageChildCard.vue').default },

    { path: '/familyPlanning', component: require('./components/familyPlanning/index.vue').default },
    { path: '/familyPlanning/Form', component: require('./components/familyPlanning/FamilyPlanningRegistrationForm.vue').default },
    { path: '/familyPlanning/Records/:id', component: require('./components/familyPlanning/FamilyPlanningRecord.vue').default },
    { path: '/familyPlanning/View/:id', component: require('./components/familyPlanning/FamilyPlanningView.vue').default },
    { path: '/familyPlanning/editFamilyPlanning/:id', component: require('./components/familyPlanning/edit/FamilyPlanning.vue').default },
    { path: '/familyPlanning/editMedicalHistory/:id', component: require('./components/familyPlanning/edit/MedicalHistory.vue').default },
    { path: '/familyPlanning/editObsterical/:id', component: require('./components/familyPlanning/edit/Obsterical.vue').default },
    { path: '/familyPlanning/editPhysical/:id', component: require('./components/familyPlanning/edit/Physical.vue').default },
    { path: '/familyPlanning/editPelvic/:id', component: require('./components/familyPlanning/edit/Pelvic.vue').default },
    { path: '/familyPlanning/editVaw/:id', component: require('./components/familyPlanning/edit/Vaw.vue').default },

    { path: '/familyProfile', component: require('./components/familyProfile/index.vue').default },
    { path: '/familyProfile/view/:id', component: require('./components/familyProfile/viewProfile.vue').default },
    { path: '/familyProfile/form/:id', component: require('./components/familyProfile/addFamily.vue').default },
    { path: '/familyProfile/form/member/:id', component: require('./components/familyProfile/addMember.vue').default },

    { path: '/individual', component: require('./components/individual/index.vue').default },
    { path: '/individual/form/:id', component: require('./components/individual/addIndividual.vue').default },
    { path: '/individual/treatment/:id', component: require('./components/individual/addTreatmentRecord.vue').default },
    { path: '/individual/view/:id', component: require('./components/individual/viewIndividual.vue').default },

    { path: '/maternal', component: require('./components/maternal/index.vue').default },
    { path: '/maternalHealth/form', component: require('./components/maternal/MaternalForm.vue').default },
    { path: '/maternalHealth/view/:id', component: require('./components/maternal/MaternalView.vue').default },
    { path: '/maternalHealth/trimester/:id', component: require('./components/maternal/MaternalTrimester.vue').default },
    { path: '/maternalHealth/tetanus/:id', component: require('./components/maternal/MaternalTetanusToxoid.vue').default },
    { path: '/maternalHealth/laboratory/:id', component: require('./components/maternal/MaternalLaboratory.vue').default },
    { path: '/maternalHealth/otherServices/:id', component: require('./components/maternal/MaternalOtherServices.vue').default },
    { path: '/maternalHealth/labor/:id', component: require('./components/maternal/MaternalLabor.vue').default },
    { path: '/maternalHealth/profileSettings/:id', component: require('./components/maternal/MaternalProfileSettings.vue').default },

    { path: '/users', component: require('./components/Users/index.vue').default },
    { path: '/user/form/:id', component: require('./components/Users/form.vue').default },

    { path: '/userSettings', component: require('./components/Users/userSettings.vue').default },
  ]

  Vue.component(
        'passport-clients',
        require('./components/passport/Clients.vue').default
    );

    Vue.component(
        'passport-authorized-clients',
        require('./components/passport/AuthorizedClients.vue').default
    );

    Vue.component(
        'passport-personal-access-tokens',
        require('./components/passport/PersonalAccessTokens.vue').default
    );

Vue.component('modal', require('./components/modal.vue').default);

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

window.Fire = new Vue();

const router = new VueRouter({
    mode: 'history',
    routes 
})

const app = new Vue({
    router
}).$mount('#app')