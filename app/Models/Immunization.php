<?php

namespace App\Models;

use App\Models\ChildHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Immunization extends Model
{
    protected $fillable = [
        'child_id',
        'bcg',
        'hepa_b',
        'nbs',
        'nbh',
        'rotavirus_first',
        'rotavirus_second',
        'rotavirus_third',
        'pentavalent_first',
        'pentavalent_second',
        'pentavalent_third',
        'pneumococcal_first',
        'pneumococcal_second',
        'pneumococcal_third',
        'measles',
        'mmr',
        'vitamin_a',
        'fic'
    ];

    public function child(): BelongsTo
    {
        return $this->belongsTo(ChildHealth::class, 'child_id');
    }
}
