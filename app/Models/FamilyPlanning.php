<?php

namespace App\Models;

use App\Models\Vaw;
use App\Models\Pelvic;
use App\Models\Physical;
use App\Models\Obsterical;
use App\Models\MedicalHistory;
use Illuminate\Database\Eloquent\Model;
use App\Models\FamilyPlanningServiceRecord;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FamilyPlanning extends Model
{
    protected $fillable = [
        'type_of_acceptor',
        'prev_method',
        'first_name',
        'middle_name',
        'last_name',
        'birthdate',
        'contact_no',
        'highest_education',
        'occupation',
        'street',
        'barangay',
        'municipality',
        'province',
        'monthly_income',
        'spouse_first_name',
        'spouse_middle_name',
        'spouse_last_name',
        'spouse_birthdate',
        'spouse_highest_education',
        'spouse_occupation',
        'number_lived_children',
        'plan_more_children',
        'reason',
        'method_accepted',
        'acknowledgment'
    ];

    public function familyPlanningServiceRecords(): HasMany
    {
        return $this->hasMany(FamilyPlanningServiceRecord::class);
    }

    public function medicalHistory(): HasOne
    {
        return $this->hasOne(MedicalHistory::class);
    } 

    public function obsterical(): HasOne
    {
        return $this->hasOne(Obsterical::class);
    }

    public function physical(): HasOne
    {
        return $this->hasOne(Physical::class);
    }

    public function pelvic(): HasOne
    {
        return $this->hasOne(Pelvic::class);
    }

    public function vaw(): HasOne
    {
        return $this->hasOne(Vaw::class);
    }
}
