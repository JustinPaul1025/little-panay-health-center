<?php

namespace App\Models;

use App\Models\ChildHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Sibling extends Model
{
    protected $fillable = [
        'child_id',
        'name',
        'birthdate'
    ];

    public function child(): BelongsTo
    {
        return $this->belongsTo(ChildHealth::class, 'child_id');
    }
}
