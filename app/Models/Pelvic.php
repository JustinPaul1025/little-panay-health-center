<?php

namespace App\Models;

use App\Models\FamilyPlanning;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Pelvic extends Model
{
    protected $fillable = [
        'family_planning_id',
        'others',
        'perinium_scars',
        'perinium_warts',
        'perinium_reddish',
        'perinium_laceration',
        'vagina_congested',
        'vagina_cyst',
        'vagina_warts',
        'vagina_skene',
        'vagina_rectocele',
        'vagina_cystocele',
        'cervic_congested',
        'cervic_erosion',
        'cervic_discharge',
        'cervic_polyps',
        'cervic_laceration',
        'consistancy_firm',
        'consistancy_soft',
        'adnex_mass',
        'adnex_tenderness',
        'uterus_position',
        'uterus_size',
        'uterus_mass',
        'uterine_depth'
    ];

    public function familyPlanning(): BelongsTo
    {
        return $this->belongsTo(FamilyPlanning::class, 'family_planning_id');
    }
}
