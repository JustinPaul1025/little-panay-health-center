<?php

namespace App\Models;

use App\Models\FamilyPlanning;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Physical extends Model
{
    protected $fillable = [
        'family_planning_id',
        'blood_pressure',
        'weight',
        'pulse_rate',
        'pale',
        'yellowish',
        'enlarge_thyroyd',
        'enlarge_lymph',
        'left_breast_mass',
        'left_breast_nipple_discharge',
        'left_breast_orange_peel',
        'left_breast_enlarge_axillary',
        'right_breast_mass',
        'right_breast_nipple_discharge',
        'right_breast_orange_peel',
        'right_breast_enlarge_axillary',
        'abnormal_heart',
        'abnormal_breath',
        'abdomen_enlarged_lever',
        'abdomen_mass',
        'abdomen_tenderness',
        'edema',
        'physical_varicosities'
    ];

    public function familyPlanning(): BelongsTo
    {
        return $this->belongsTo(FamilyPlanning::class, 'family_planning_id');
    }
}
