<?php

namespace App\Models;

use App\Models\FamilyPlanning;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Obsterical extends Model
{
    protected $fillable = [
        'family_planning_id',
        'full_term',
        'premature',
        'abortions',
        'living_children',
        'date_last_delivery',
        'type_last_delivery',
        'past_menstrual_periods',
        'last_menstrual_periods',
        'duration_menstrual_perios'
    ];

    public function familyPlanning(): BelongsTo
    {
        return $this->belongsTo(FamilyPlanning::class, 'family_planning_id');
    }
}
