<?php

namespace App\Models;

use App\Models\FamilyPlanning;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FamilyPlanningServiceRecord extends Model
{
    protected $fillable = [
        'family_planning_id',
        'method_brand',
        'number_of_units',
        'remarks',
        'provider_name',
        'next_service'
    ];

    public function familyPlanning(): BelongsTo
    {
        return $this->belongsTo(FamilyPlanning::class, 'family_planning_id');
    }
}
