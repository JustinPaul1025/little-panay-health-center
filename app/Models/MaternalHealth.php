<?php

namespace App\Models;

use App\Models\MaternalHealthLabor;
use App\Models\MaternalHealthIronTab;
use App\Models\MaternalHealthTrimester;
use Illuminate\Database\Eloquent\Model;
use App\Models\MaternalHealthLaboratory;
use App\Models\MaternalHealthObsterical;
use App\Models\MaternalHealthDateOfVisit;
use App\Models\MaternalHealthOtherService;
use App\Models\MaternalHealthDateNextVisit;
use App\Models\MaternalHealthFamilyHistory;
use App\Models\MaternalHealthTetanusStatus;
use App\Models\MaternalHealthPreviousPregnancy;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class MaternalHealth extends Model
{
    protected $fillable = [
        'nhts_no',
        'ips_no',
        'phic_no',
        'name',
        'birthdate',
        'address',
        'contact_no',
        'civil_status',
        'place_of_birth',
        'occupation',
        'gravida',
        'para',
        'abortion',
        'date_last_delivery',
        'lmp',
        'edc',
        'husband_name',
        'husband_birthdate',
        'husband_occupation'
    ];

    public function trimesters(): HasMany
    {
        return $this->hasMany(MaternalHealthTrimester::class);
    }

    public function others(): HasMany
    {
        return $this->hasMany(MaternalHealthOtherService::class);
    }

    public function ironTabs(): HasMany
    {
        return $this->hasMany(MaternalHealthIronTab::class);
    }

    public function laboratories(): HasMany
    {
        return $this->hasMany(MaternalHealthLaboratory::class);
    }

    public function obsterical(): HasOne
    {
        return $this->hasOne(MaternalHealthObsterical::class);
    }

    public function prevPregnancy(): HasOne
    {
        return $this->hasOne(MaternalHealthPreviousPregnancy::class);
    }

    public function familyHistory(): HasOne
    {
        return $this->hasOne(MaternalHealthFamilyHistory::class);
    }

    public function tetanusStatus(): HasOne
    {
        return $this->hasOne(MaternalHealthTetanusStatus::class);
    }

    public function labor(): HasOne
    {
        return $this->hasOne(MaternalHealthLabor::class);
    }

    public function nextVisit(): HasOne
    {
        return $this->hasOne(MaternalHealthDateNextVisit::class);
    }

    public function dateOfVisit(): HasOne
    {
        return $this->hasOne(MaternalHealthDateOfVisit::class);
    }
}
