<?php

namespace App\Models;

use App\Models\Member;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FamilyProfile extends Model
{
    protected $fillable = [
        'name',
        'address'
    ];

    public function members(): HasMany
    {
        return $this->hasMany(Member::class);
    }
}
