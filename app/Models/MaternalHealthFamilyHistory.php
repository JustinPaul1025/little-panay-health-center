<?php

namespace App\Models;

use App\Models\MaternalHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MaternalHealthFamilyHistory extends Model
{
    protected $fillable = [
        'maternal_health_id',
        'special_disease',
        'specific_special_disease',
        'special_case',
        'specific_special_case'
    ];

    public function maternalHealth(): BelongsTo
    {
        return $this->belongsTo(MaternalHealth::class);
    }
}
