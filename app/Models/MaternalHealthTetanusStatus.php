<?php

namespace App\Models;

use App\Models\MaternalHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MaternalHealthTetanusStatus extends Model
{
    protected $fillable = [
        'maternal_health_id',
        'tt1',
        'tt2',
        'tt3',
        'tt4',
        'tt5',
        'ttl'
    ];

    public function maternalHealth(): BelongsTo
    {
        return $this->belongsTo(MaternalHealth::class);
    }
}
