<?php

namespace App\Models;

use App\Models\MaternalHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MaternalHealthPreviousPregnancy extends Model
{
    protected $fillable = [
        'maternal_health_id',
        'hemorrhage',
        'toxemia',
        'placenta_Previa',
        'sepsis',
        'non_obsterical',
        'others'
    ];

    public function maternalHealth(): BelongsTo
    {
        return $this->belongsTo(MaternalHealth::class);
    }
}
