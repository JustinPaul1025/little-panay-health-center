<?php

namespace App\Models;

use App\Models\Sibling;
use App\Models\BreastFeed;
use App\Models\ChildRecord;
use App\Models\Immunization;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ChildHealth extends Model
{
    protected $fillable = [
        'fsn_no',
        'nhts_no',
        'phic_no',
        'first_name',
        'middle_name',
        'last_name',
        'birthdate',
        'contact_no',
        'address',
        'mother_name',
        'mother_birthdate',
        'mother_occupation',
        'father_name',
        'father_birthdate',
        'father_occupation',
        'tt_status',
        'birth_order',
        'gender',
        'ccr_register_date',
        'tcl_register_date',
        'marital_status',
        'delivery',
        'abnormalities',
        'birth_weight',
        'birth_length',
        'place_of_birth',
        'breast_feeding',
        'reason'
    ];

    public function childRecords(): HasMany
    {
        return $this->hasMany(ChildRecord::class);
    }

    public function siblings(): HasMany
    {
        return $this->hasMany(Sibling::class);
    }    

    public function immunization(): HasOne
    {
        return $this->hasOne(Immunization::class);
    }
    
    public function breastFeed(): HasOne
    {
        return $this->hasOne(BreastFeed::class);
    }    
}
