<?php

namespace App\Models;

use App\Models\FamilyPlanning;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MedicalHistory extends Model
{
    protected $fillable = [
        'family_planning_id',
        'epilepsy',
        'dizziness',
        'blur_vision',
        'conjuctiva',
        'thyroid',
        'chest_pain',
        'fatigability',
        'axillary',
        'nipple_discharge',
        'systolic',
        'diastolic',
        'cva_history',
        'mass_abdomen',
        'gallbladder_disease',
        'liver_disease',
        'mass_uterus',
        'vaginal_discharge',
        'intermenstrual_bleeding',
        'postcoital_bleeding',
        'varicosities',
        'severe_pain',
        'yellowish_skin',
        'smoking',
        'allergies',
        'drug_intake',
        'bleeding_tendencies',
        'anemia',
        'diabetes',
        'hydatidiform',
        'ectopic_pregnancy'
    ];

    public function familyPlanning(): BelongsTo
    {
        return $this->belongsTo(FamilyPlanning::class, 'family_planning_id');
    }
}
