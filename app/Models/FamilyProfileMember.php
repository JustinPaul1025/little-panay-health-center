<?php

namespace App\Models;

use App\Models\FamilyProfile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FamilyProfileMember extends Model
{
    protected $fillable = [
        'family_id',
        'name',
        'gender',
        'birthdate',
        'relationship'
    ];

    public function family(): BelongsTo
    {
        return $this->belongsTo(FamilyProfile::class, 'family_id');
    }
}
