<?php

namespace App\Models;

use App\Models\MaternalHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MaternalHealthDateNextVisit extends Model
{
    protected $fillable = [
        'maternal_health_id',
        'cell1',
        'cell2',
        'cell3',
        'cell4',
        'cell5',
        'cell6',
        'cell7',
        'cell8'
    ];

    public function maternalHealth(): BelongsTo
    {
        return $this->belongsTo(MaternalHealth::class);
    }
}
