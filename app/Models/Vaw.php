<?php

namespace App\Models;

use App\Models\FamilyPlanning;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Vaw extends Model
{
    protected $fillable = [
        'family_planning_id',
        'vaw_history',
        'unpleasant_relation',
        'partner_not_approved',
        'partner_disagree',
        'reffered_to',
        'other'
    ];

    public function familyPlanning(): BelongsTo
    {
        return $this->belongsTo(FamilyPlanning::class, 'family_planning_id');
    }
}
