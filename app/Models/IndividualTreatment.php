<?php

namespace App\Models;

use App\Models\TreatmentRecord;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class IndividualTreatment extends Model
{
    protected $fillable = [
        'name',
        'contact_no',
        'birthdate',
        'gender',
        'civil_status',
        'address',
        'occupation',
        'philhealth',
        'spouse_name',
        'spouse_birthdate',
        'spouse_occupation',
        'father_name',
        'father_birthdate',
        'father_occupation',
        'mother_name',
        'mother_birthdate',
        'mother_occupation',
    ];

    public function treatmentRecord(): HasMany
    {
        return $this->hasMany(TreatmentRecord::class);
    }
}
