<?php

namespace App\Models;

use App\Models\MaternalHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MaternalHealthLabor extends Model
{
    protected $fillable = [
        'maternal_health_id',
        'immediate_breastfeeding',
        'type_of_delivery',
        'date_of_delivery',
        'place_of_delivery',
        'birth_attendant',
        'birth_weight',
        'hemorhage',
        'alive',
        'healthy'
    ];

    public function maternalHealth(): BelongsTo
    {
        return $this->belongsTo(MaternalHealth::class);
    }
}
