<?php

namespace App\Models;

use App\Models\MaternalHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MaternalHealthTrimester extends Model
{
    protected $fillable = [
        'maternal_health_id',
        'timester_type',
        'cell_1',
        'cell_2',
        'cell_3',
        'cell_4',
        'cell_5',
        'cell_6',
        'cell_7',
        'cell_8',
        'cell_9'
    ];

    public function maternalHealth(): BelongsTo
    {
        return $this->belongsTo(MaternalHealth::class);
    }
}
