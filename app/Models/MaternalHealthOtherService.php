<?php

namespace App\Models;

use App\Models\MaternalHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MaternalHealthOtherService extends Model
{
    protected $fillable = [
        'maternal_health_id',
        'service_id',
        'action',
        'remarks'
    ];

    public function maternalHealth(): BelongsTo
    {
        return $this->belongsTo(MaternalHealth::class);
    }
}
