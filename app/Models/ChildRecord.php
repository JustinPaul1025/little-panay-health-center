<?php

namespace App\Models;

use App\Models\ChildHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ChildRecord extends Model
{
    protected $fillable = [
        'child_id',
        'weight_status',
        'height_status',
        'remarks'
    ];

    public function child(): BelongsTo
    {
        return $this->belongsTo(ChildHealth::class, 'child_id');
    }
}
