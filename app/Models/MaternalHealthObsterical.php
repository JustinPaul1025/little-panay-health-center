<?php

namespace App\Models;

use App\Models\MaternalHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MaternalHealthObsterical extends Model
{
    protected $fillable = [
        'maternal_health_id',
        'born_alive',
        'living_child',
        'abortion',
        'fetal_death',
        'death',
        'deliver_cs'
    ];

    public function maternalHealth(): BelongsTo
    {
        return $this->belongsTo(MaternalHealth::class);
    }
}
