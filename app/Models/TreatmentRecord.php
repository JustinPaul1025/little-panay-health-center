<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TreatmentRecord extends Model
{
    protected $fillable = [
        'individual_id',
        'date_treated',
        'complaints',
        'service_rendered'
    ];

    public function individualTreatment(): BelongsTo
    {
        return $this->belongsTo(IndividualTreatment::class, 'family_id');
    }
}
