<?php

namespace App\Models;

use App\Models\ChildHealth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BreastFeed extends Model
{
    protected $fillable = [
        'child_id',
        'first_month',
        'second_month',
        'third_month',
        'fourth_month',
        'fifth_month',
        'sixth_month',
        'seventh_month',
        'eight_month'
    ];

    public function child(): BelongsTo
    {
        return $this->belongsTo(ChildHealth::class, 'child_id');
    }
}
