<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\Obsterical;
use Illuminate\Http\Request;
use App\Models\FamilyPlanning;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class ObstericalController extends Controller
{
    public function show($id): JsonResponse
    {
        return response()->json(FamilyPlanning::find($id)->obsterical);
    }

    public function update(Request $request, Obsterical $obsterical)
    {
        $obsterical
        ->update([
            'full_term' => $request->input('full_term'),
            'premature' => $request->input('premature'),
            'abortions' => $request->input('abortions'),
            'living_children' => $request->input('living_children'),
            'date_last_delivery' => Carbon::parse($request->input('date_last_delivery')),
            'type_last_delivery' => $request->input('type_last_delivery'),
            'past_menstrual_periods' => $request->input('past_menstrual_periods'),
            'last_menstrual_periods' => $request->input('last_menstrual_periods'),
            'duration_menstrual_perios' => $request->input('duration_menstrual_perios')
        ]);

        return response()->json($obsterical);
    }

}
