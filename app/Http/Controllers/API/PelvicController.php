<?php

namespace App\Http\Controllers\API;

use App\Models\Pelvic;
use Illuminate\Http\Request;
use App\Models\FamilyPlanning;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class PelvicController extends Controller
{

    public function show($id): JsonResponse
    {
        return response()->json(FamilyPlanning::find($id)->pelvic);
    }

    public function update(Request $request, Pelvic $pelvic): JsonResponse
    {
        $pelvic
        ->update([
            'others' => $request->input('others'),
            'perinium_scars' => $request->input('perinium_scars'),
            'perinium_warts' => $request->input('perinium_warts'),
            'perinium_reddish' => $request->input('perinium_reddish'),
            'perinium_laceration' => $request->input('perinium_laceration'),
            'vagina_congested' => $request->input('vagina_congested'),
            'vagina_cyst' => $request->input('vagina_cyst'),
            'vagina_warts' => $request->input('vagina_warts'),
            'vagina_skene' => $request->input('vagina_skene'),
            'vagina_rectocele' => $request->input('vagina_rectocele'),
            'vagina_cystocele' => $request->input('vagina_cystocele'),
            'cervic_congested' => $request->input('cervic_congested'),
            'cervic_erosion' => $request->input('cervic_erosion'),
            'cervic_discharge' => $request->input('cervic_discharge'),
            'cervic_polyps' => $request->input('cervic_polyps'),
            'cervic_laceration' => $request->input('cervic_laceration'),
            'consistancy_firm' => $request->input('consistancy_firm'),
            'consistancy_soft' => $request->input('consistancy_soft'),
            'adnex_mass' => $request->input('adnex_mass'),
            'adnex_tenderness' => $request->input('adnex_tenderness'),
            'uterus_position' => $request->input('uterus_position'),
            'uterus_size' => $request->input('uterus_size'),
            'uterus_mass' => $request->input('uterus_mass'),
            'uterine_depth' => $request->input('uterine_depth')
        ]);

        return response()->json($pelvic);
    }

}
