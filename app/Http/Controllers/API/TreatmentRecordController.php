<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TreatmentRecord;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class TreatmentRecordController extends Controller
{
    public function getIndividualRecords($id): JsonResponse
    {
        return response()->json(TreatmentRecord::where('individual_id', $id)->get());
    }

    public function store(Request $request): JsonResponse
    {
        $currentData = TreatmentRecord::create([
            'individual_id' => $request->input('individual_id'),
            'date_treated' => Carbon::parse($request->input('date_treated')),
            'complaints' => $request->input('complaints'),
            'service_rendered' => $request->input('service_rendered')
        ]);

        return response()->json($currentData);
    }

    public function update(Request $request, TreatmentRecord $treatmentRecord): JsonResponse
    {
        $treatmentRecord
          ->update([
                'date_treated' => Carbon::parse($request->input('date_treated')),
                'complaints' => $request->input('complaints'),
                'service_rendered' => $request->input('service_rendered')
            ]);
        
        return response()->json($treatmentRecord);
    }

    public function destroy(TreatmentRecord $treatmentRecord): JsonResponse
    {
        return response()->json($treatmentRecord->delete());
    }
}
