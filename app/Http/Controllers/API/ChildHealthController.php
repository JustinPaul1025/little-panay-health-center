<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\Sibling;
use App\Models\BreastFeed;
use App\Models\ChildHealth;
use App\Models\Immunization;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class ChildHealthController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(ChildHealth::latest()->paginate(10));
    }

    public function store(Request $request): JsonResponse
    {
        $currentData = ChildHealth::create([
            'fsn_no' => $request->input('fsn_no'),
            'nhts_no' => $request->input('nhts_no'),
            'phic_no' => $request->input('phic_no'),
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'birthdate' => Carbon::parse($request->input('birthdate')),
            'contact_no' => $request->input('contact_no'),
            'address' => $request->input('address'),
            'mother_name' => $request->input('mother_name'),
            'mother_birthdate' => Carbon::parse($request->input('mother_birthdate')),
            'mother_occupation' => $request->input('mother_occupation'),
            'father_name' => $request->input('father_name'),
            'father_birthdate' => Carbon::parse($request->input('father_birthdate')),
            'father_occupation' => $request->input('father_occupation'),
            'tt_status' => Carbon::parse($request->input('tt_status')),
            'birth_order' => $request->input('birth_order'),
            'gender' => $request->input('gender'),
            'ccr_register_date' => Carbon::parse($request->input('ccr_register_date')),
            'tcl_register_date' => Carbon::parse($request->input('tcl_register_date')),
            'marital_status' => $request->input('marital_status'),
            'delivery' => $request->input('delivery'),
            'abnormalities' => $request->input('abnormalities'),
            'birth_weight' => $request->input('birth_weight'),
            'birth_length' => $request->input('birth_length'),
            'place_of_birth' => $request->input('place_of_birth'),
            'breast_feeding' => $request->input('breast_feeding'),
            'reason' => $request->input('reason')
        ]);
        

        foreach ($request->input('siblings') as $sibling) {
            Sibling::create([
                'child_id' => $currentData->id,
                'name' => $sibling['name'],
                'birthdate' => Carbon::parse($sibling['birthdate'])
            ]);
        }

        Immunization::create([
            'child_id' => $currentData->id
        ]);

        BreastFeed::create([
            'child_id' => $currentData->id
        ]);

        return response()->json($currentData);
    }

    public function show(ChildHealth $childHealth): JsonResponse
    {
        return response()->json($childHealth);
    }

    public function update(Request $request, ChildHealth $childHealth): JsonResponse
    {
        $childHealth
            ->update([
                'fsn_no' => $request->input('fsn_no'),
                'nhts_no' => $request->input('nhts_no'),
                'phic_no' => $request->input('phic_no'),
                'first_name' => $request->input('first_name'),
                'middle_name' => $request->input('middle_name'),
                'last_name' => $request->input('last_name'),
                'birthdate' => Carbon::parse($request->input('birthdate')),
                'contact_no' => $request->input('contact_no'),
                'address' => $request->input('address'),
                'mother_name' => $request->input('mother_name'),
                'mother_birthdate' => Carbon::parse($request->input('mother_birthdate')),
                'mother_occupation' => $request->input('mother_occupation'),
                'father_name' => $request->input('father_name'),
                'father_birthdate' => Carbon::parse($request->input('father_birthdate')),
                'father_occupation' => $request->input('father_occupation'),
                'tt_status' => Carbon::parse($request->input('tt_status')),
                'birth_order' => $request->input('birth_order'),
                'gender' => $request->input('gender'),
                'ccr_register_date' => Carbon::parse($request->input('ccr_register_date')),
                'tcl_register_date' => Carbon::parse($request->input('tcl_register_date')),
                'marital_status' => $request->input('marital_status'),
                'delivery' => $request->input('delivery'),
                'abnormalities' => $request->input('abnormalities'),
                'birth_weight' => $request->input('birth_weight'),
                'birth_length' => $request->input('birth_length'),
                'place_of_birth' => $request->input('place_of_birth'),
                'breast_feeding' => $request->input('breast_feeding'),
                'reason' => $request->input('reason')
            ]);

        return response()->json($childHealth);
    }

    public function destroy(ChildHealth $childHealth): JsonResponse
    {
        return response()->json($childHealth->delete());
    }

    public function search($keyword): JsonResponse
    {
        $response = ChildHealth::where('first_name', 'like', "%{$keyword}%")
            ->orWhere('last_name', 'like', "%{$keyword}%")
            ->paginate(10);

        return response()->json($response);
    }
}
