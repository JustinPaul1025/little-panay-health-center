<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function profile(): JsonResponse
    {
        return response()->json(auth('api')->user());
    }

    public function search($keyword)
    {
        $response = User::where('first_name', 'like', "%{$keyword}%")
            ->orWhere('last_name', 'like', "%{$keyword}%")
            ->orWhere('username', 'like', "%{$keyword}%")
            ->get();

        return response()->json($response);
    }


    public function index(): JsonResponse
    {
        return response()->json(User::latest()->paginate(10));
    }

    public function store(Request $request): JsonResponse
    {
        return response()->json(User::create([
            'username' => $request->input('username'),
            'role' => $request->input('role'),
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'password' => Hash::make($request->input('password'))
        ]));
    }

    public function show(User $user)
    {
        return response()->json($user);
    }

    public function update(Request $request, User $user)
    {
        $user
            ->update([
            'username' => $request->input('username'),
            'role' => $request->input('role'),
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
        ]);

        return response()->json($user); 
    }

    public function destroy(User $user): JsonResponse
    {
        return response()->json($user->delete());
    }

    public function changePassword(Request $request, User $user)
    {
        $user
            ->update([
                'password' => Hash::make($request->input('password'))
            ]);

        return response()->json($user);
    }
}
