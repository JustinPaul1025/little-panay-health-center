<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\FamilyProfileMember;
use App\Http\Controllers\Controller;

class FamilyProfileMemberController extends Controller
{
    public function getFamilyMembers($id): JsonResponse
    {
        return response()->json(FamilyProfileMember::where('family_id', $id)->get());
    }

    public function store(Request $request): JsonResponse
    {
        $currentData = FamilyProfileMember::create([
            'family_id' => $request->input('family_id'),
            'name' => $request->input('name'),
            'gender' => $request->input('gender'),
            'birthdate' => Carbon::parse($request->input('birthdate')),
            'relationship' => $request->input('relationship')
        ]);

        return response()->json($currentData);
    }

    public function update(Request $request, FamilyProfileMember $familyProfileMember): JsonResponse
    {

        $familyProfileMember
          ->update([
                'name' => $request->input('name'),
                'gender' => $request->input('gender'),
                'birthdate' => Carbon::parse($request->input('birthdate')),
                'relationship' => $request->input('relationship')
            ]);
        
        return response()->json($familyProfileMember);
    }

    public function destroy(FamilyProfileMember $familyProfileMember): JsonResponse
    {
        return response()->json($familyProfileMember->delete());
    }
}
