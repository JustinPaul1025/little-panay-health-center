<?php

namespace App\Http\Controllers\API;

use App\Models\Vaw;
use Illuminate\Http\Request;
use App\Models\FamilyPlanning;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class VawController extends Controller
{
    public function show($id): JsonResponse
    {
        return response()->json(FamilyPlanning::find($id)->vaw);
    }

    public function update(Request $request, Vaw $vaw): JsonResponse
    {
        $vaw
        ->update([
            'vaw_history' => $request->input('vaw_history'),
            'unpleasant_relation' => $request->input('unpleasant_relation'),
            'partner_not_approved' => $request->input('partner_not_approved'),
            'partner_disagree' => $request->input('partner_disagree'),
            'reffered_to' => $request->input('reffered_to'),
            'other' => $request->input('other')
        ]);

        return response()->json($vaw);
    }

}
