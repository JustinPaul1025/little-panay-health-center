<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\Immunization;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class ImmunizationController extends Controller
{
    public function show($id): JsonResponse
    {
        return response()->json(Immunization::where('child_id', $id)->first());
    }

    public function update(Request $request, Immunization $immunization): JsonResponse
    {
        
        $immunization
            ->update([
                'bcg' => $request->input('bcg'),
                'hepa_b' => $request->input('hepa_b'),
                'nbs' => $request->input('nbs'),
                'nbh' => $request->input('nbh'),
                'rotavirus_first' => $request->input('rotavirus_first'),
                'rotavirus_second' => $request->input('rotavirus_second'),
                'rotavirus_third' => $request->input('rotavirus_third'),
                'pentavalent_first' => $request->input('pentavalent_first'),
                'pentavalent_second' => $request->input('pentavalent_second'),
                'pentavalent_third' => $request->input('pentavalent_third'),
                'pneumococcal_first' => $request->input('pneumococcal_first'),
                'pneumococcal_second' => $request->input('pneumococcal_second'),
                'pneumococcal_third' => $request->input('pneumococcal_third'),
                'measles' => $request->input('measles'),
                'mmr' => $request->input('mmr'),
                'vitamin_a' => $request->input('vitamin_a'),
                'fic' => $request->input('fic')
            ]);

        return response()->json($immunization);
    }

}
