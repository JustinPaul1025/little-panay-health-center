<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\FamilyPlanning;
use App\Models\MedicalHistory;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class MedicalHistoryController extends Controller
{

    public function show($id): JsonResponse
    {
        $medicalHistory = FamilyPlanning::find($id)->medicalHistory;
        return response()->Json($medicalHistory);
    }

    public function update(Request $request, MedicalHistory $medicalHistory)
    {
        $medicalHistory
            ->update([
                'epilepsy' => $request->input('epilepsy'),
                'dizziness' => $request->input('dizziness'),
                'blur_vision' => $request->input('blur_vision'),
                'conjuctiva' => $request->input('conjuctiva'),
                'thyroid' => $request->input('thyroid'),
                'chest_pain' => $request->input('chest_pain'),
                'fatigability' => $request->input('fatigability'),
                'axillary' => $request->input('axillary'),
                'nipple_discharge' => $request->input('nipple_discharge'),
                'systolic' => $request->input('systolic'),
                'diastolic' => $request->input('diastolic'),
                'cva_history' => $request->input('cva_history'),
                'mass_abdomen' => $request->input('mass_abdomen'),
                'gallbladder_disease' => $request->input('gallbladder_disease'),
                'liver_disease' => $request->input('liver_disease'),
                'mass_uterus' => $request->input('mass_uterus'),
                'vaginal_discharge' => $request->input('vaginal_discharge'),
                'intermenstrual_bleeding' => $request->input('intermenstrual_bleeding'),
                'postcoital_bleeding' => $request->input('postcoital_bleeding'),
                'varicosities' => $request->input('varicosities'),
                'severe_pain' => $request->input('severe_pain'),
                'yellowish_skin' => $request->input('yellowish_skin'),
                'smoking' => $request->input('smoking'),
                'allergies' => $request->input('allergies'),
                'drug_intake' => $request->input('drug_intake'),
                'bleeding_tendencies' => $request->input('bleeding_tendencies'),
                'anemia' => $request->input('anemia'),
                'diabetes' => $request->input('diabetes'),
                'hydatidiform' => $request->input('hydatidiform'),
                'ectopic_pregnancy' => $request->input('ectopic_pregnancy')
            ]);

        return response()->json($medicalHistory);
    }

}
