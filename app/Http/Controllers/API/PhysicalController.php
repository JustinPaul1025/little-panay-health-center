<?php

namespace App\Http\Controllers\API;

use App\Models\Physical;
use Illuminate\Http\Request;
use App\Models\FamilyPlanning;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class PhysicalController extends Controller
{
    public function show($id): JsonResponse
    {
        $data = Physical::where('family_planning_id', $id)->first();
        return response()->json($data);
    }

    public function update(Request $request, Physical $physical): JsonResponse
    {
        $physical
        ->update([
            'blood_pressure' => $request->input('blood_pressure'),
            'weight' => $request->input('weight'),
            'pulse_rate' => $request->input('pulse_rate'),
            'pale' => $request->input('pale'),
            'yellowish' => $request->input('yellowish'),
            'enlarge_thyroyd' => $request->input('enlarge_thyroyd'),
            'enlarge_lymph' => $request->input('enlarge_lymph'),
            'left_breast_mass' => $request->input('left_breast_mass'),
            'left_breast_nipple_discharge' => $request->input('left_breast_nipple_discharge'),
            'left_breast_orange_peel' => $request->input('left_breast_orange_peel'),
            'left_breast_enlarge_axillary' => $request->input('left_breast_enlarge_axillary'),
            'right_breast_mass' => $request->input('right_breast_mass'),
            'right_breast_nipple_discharge' => $request->input('right_breast_nipple_discharge'),
            'right_breast_orange_peel' => $request->input('right_breast_orange_peel'),
            'right_breast_enlarge_axillary' => $request->input('right_breast_enlarge_axillary'),
            'abnormal_heart' => $request->input('abnormal_heart'),
            'abnormal_breath' => $request->input('abnormal_breath'),
            'abdomen_enlarged_lever' => $request->input('abdomen_enlarged_lever'),
            'abdomen_mass' => $request->input('abdomen_mass'),
            'abdomen_tenderness' => $request->input('abdomen_tenderness'),
            'edema' => $request->input('edema'),
            'physical_varicosities' => $request->input('physical_varicosities')
        ]);

        return response()->json($physical);
    }

}
