<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\MaternalHealth;
use Illuminate\Http\JsonResponse;
use App\Models\MaternalHealthLabor;
use App\Http\Controllers\Controller;
use App\Models\MaternalHealthIronTab;
use App\Models\MaternalHealthTrimester;
use App\Models\MaternalHealthLaboratory;
use App\Models\MaternalHealthObsterical;
use App\Models\MaternalHealthDateOfVisit;
use App\Models\MaternalHealthOtherService;
use App\Models\MaternalHealthDateNextVisit;
use App\Models\MaternalHealthFamilyHistory;
use App\Models\MaternalHealthTetanusStatus;
use App\Models\MaternalHealthPreviousPregnancy;

class MaternalHealthController extends Controller
{
    public function index(): JsonResponse
    {

        return response()->json(MaternalHealth::latest()->paginate(10));

    }

    public function store(Request $request): JsonResponse
    {

        $currentData = MaternalHealth::create([
            'nhts_no' => $request->input('nhts_no'),
            'ips_no' => $request->input('ips_no'),
            'phic_no' => $request->input('phic_no'),
            'name' => $request->input('name'),
            'birthdate' => $request->input('birthdate'),
            'address' => $request->input('address'),
            'contact_no' => $request->input('contact_no'),
            'civil_status' => $request->input('civil_status'),
            'place_of_birth' => $request->input('place_of_birth'),
            'occupation' => $request->input('occupation'),
            'gravida' => $request->input('gravida'),
            'para' => $request->input('para'),
            'abortion' => $request->input('abortion'),
            'date_last_delivery' => $request->input('date_last_delivery'),
            'lmp' => $request->input('lmp'),
            'edc' => $request->input('edc'),
            'husband_name' => $request->input('husband_name' ),
            'husband_birthdate' => $request->input('husband_birthdate'),
            'husband_occupation' => $request->input('husband_occupation')
        ]);

        MaternalHealthObsterical::create([
            'maternal_health_id' => $currentData->id,
            'born_alive' => $request->input('born_alive'),
            'living_child' => $request->input('living_child'),
            'abortion' => $request->input('obsterical_abortion'),
            'fetal_death' => $request->input('fetal_death'),
            'death' => $request->input('death'),
            'deliver_cs' => $request->input('deliver_cs')
        ]);

        MaternalHealthPreviousPregnancy::create([
            'maternal_health_id' => $currentData->id,
            'hemorrhage' => $request->input('hemorrhage'),
            'toxemia' => $request->input('toxemia'),
            'placenta_Previa' => $request->input('placenta_Previa'),
            'sepsis' => $request->input('sepsis'),
            'non_obsterical' => $request->input('non_obsterical'),
            'others' => $request->input('others')
        ]);

        MaternalHealthFamilyHistory::create([
            'maternal_health_id' => $currentData->id,
            'special_disease' => $request->input('special_disease'),
            'specific_special_disease' => $request->input('specific_special_disease'),
            'special_case' => $request->input('special_case'),
            'specific_special_case' => $request->input('specific_special_case'),
        ]);

        MaternalHealthDateOfVisit::create([
            'maternal_health_id' => $currentData->id
        ]);

        for ($x = 1; $x < 14; $x++) {
            MaternalHealthTrimester::create([
                'maternal_health_id' => $currentData->id,
                'timester_type' => $x
            ]);
        }

        MaternalHealthTetanusStatus::create([
            'maternal_health_id' => $currentData->id
        ]);

        for ($x = 1; $x < 7; $x++) {
            MaternalHealthLaboratory::create([
                'maternal_health_id' => $currentData->id,
                'laboratory_type' => $x
            ]);
        }

        for ($x = 1; $x < 9; $x++) {
            MaternalHealthOtherService::create([
                'maternal_health_id' => $currentData->id,
                'service_id' => $x
            ]);
        }

        MaternalHealthLabor::create([
            'maternal_health_id' => $currentData->id
        ]);

        MaternalHealthDateNextVisit::create([
            'maternal_health_id' => $currentData->id
        ]);

        return response()->json($currentData);

    }

    public function show(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth);

    }

    public function update(Request $request, MaternalHealth $maternalHealth): JsonResponse
    {
        $maternal = $request->input('maternal');

        $obsterical = $request->input('obsterical');

        $prevPregnancy = $request->input('prev_pregnancy');

        $history = $request->input('family_history');

        $maternalHealth->update([
            'nhts_no' => $maternal['nhts_no'],
            'ips_no' => $maternal['ips_no'],
            'phic_no' => $maternal['phic_no'],
            'name' => $maternal['name'],
            'birthdate' => $maternal['birthdate'],
            'address' => $maternal['address'],
            'contact_no' => $maternal['contact_no'],
            'civil_status' => $maternal['civil_status'],
            'place_of_birth' => $maternal['place_of_birth'],
            'occupation' => $maternal['occupation'],
            'gravida' => $maternal['gravida'],
            'para' => $maternal['para'],
            'abortion' => $maternal['abortion'],
            'date_last_delivery' => $maternal['date_last_delivery'],
            'lmp' => $maternal['lmp'],
            'edc' => $maternal['edc'],
            'husband_name' => $maternal['husband_name' ],
            'husband_birthdate' => $maternal['husband_birthdate'],
            'husband_occupation' => $maternal['husband_occupation']
        ]);

        $maternalHealth->obsterical()
        ->update([
            'born_alive' => $obsterical['born_alive'],
            'living_child' => $obsterical['living_child'],
            'abortion' => $obsterical['abortion'],
            'fetal_death' => $obsterical['fetal_death'],
            'death' => $obsterical['death'],
            'deliver_cs' => $obsterical['deliver_cs']
        ]);

        $maternalHealth->prevPregnancy()
        ->update([
            'hemorrhage' => $prevPregnancy['hemorrhage'],
            'toxemia' => $prevPregnancy['toxemia'],
            'placenta_Previa' => $prevPregnancy['placenta_Previa'],
            'sepsis' => $prevPregnancy['sepsis'],
            'non_obsterical' => $prevPregnancy['non_obsterical'],
            'others' => $prevPregnancy['others']
        ]);

        $maternalHealth->familyHistory()
        ->update([
            'special_disease' => $history['special_disease'],
            'specific_special_disease' => $history['specific_special_disease'],
            'special_case' => $history['special_case'],
            'specific_special_case' => $history['specific_special_case'],
        ]);

        return response()->json($maternalHealth);
    }

    public function destroy(MaternalHealth $maternalHealth): JsonResponse
    {
        return response()->json($maternalHealth->delete());
    }

    public function obsterical(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->obsterical);

    }

    public function prev_pregnancy(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->prevPregnancy);

    }

    public function family_history(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->familyHistory);

    }

    public function trimesters(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->trimesters);

    }

    public function date_visit(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->dateOfVisit);

    }

    public function updateTrimester(Request $request, MaternalHealth $maternalHealth): JsonResponse
    {

        foreach ($request->input('trimesters') as $trimester) {
            $maternalHealth->trimesters()->updateOrCreate(
                [
                    'id' => $trimester['id'] ?? null
                ],
                [
                    'timester_type' => $trimester['timester_type'],
                    'cell_1' => $trimester['cell_1'],
                    'cell_2' => $trimester['cell_2'],
                    'cell_3' => $trimester['cell_3'],
                    'cell_4' => $trimester['cell_4'],
                    'cell_5' => $trimester['cell_5'],
                    'cell_6' => $trimester['cell_6'],
                    'cell_7' => $trimester['cell_7'],
                    'cell_8' => $trimester['cell_8'],
                    'cell_9' => $trimester['cell_9']
                ]
            );
        }

        return response()->json($maternalHealth);

    }

    public function getTetanus(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->tetanusStatus);

    }

    public function updateTetanus(Request $request, MaternalHealth $maternalHealth): JsonResponse
    {

        $maternalHealth->tetanusStatus()->update([
            'tt1' => $request->input('tt1'),
            'tt2' => $request->input('tt2'),
            'tt3' => $request->input('tt3'),
            'tt4' => $request->input('tt4'),
            'tt5' => $request->input('tt5'),
            'ttl' => $request->input('ttl')
        ]);

        return response()->Json($maternalHealth);

    }

    public function getLaboratories(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->laboratories);

    }

    public function updateLaboratories(Request $request, MaternalHealth $maternalHealth): JsonResponse
    {

        foreach ($request->input('laboratories') as $laboratory) {
            $maternalHealth->laboratories()->updateOrCreate(
                [
                    'id' => $laboratory['id'] ?? null
                ],
                [
                    'laboratory_type' => $laboratory['laboratory_type'],
                    'date' => $laboratory['date'],
                    'result' => $laboratory['result']
                ]
            );
        }

        return response()->json($maternalHealth);

    }

    public function getServices(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->others);

    }

    public function updateServices(Request $request, MaternalHealth $maternalHealth): JsonResponse
    {

        foreach ($request->input('services') as $service) {
            $maternalHealth->others()->updateOrCreate(
                [
                    'id' => $service['id'] ?? null
                ],
                [
                    'service_id' => $service['service_id'],
                    'action' => $service['action'],
                    'remarks' => $service['remarks']
                ]
            );
        }

        return response()->json($maternalHealth);

    }

    public function getDelivery(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->labor);

    }

    public function updateDelivery(Request $request, MaternalHealth $maternalHealth): JsonResponse
    {

        $maternalHealth->labor()->update([
            'immediate_breastfeeding' => $request->input('immediate_breastfeeding'),
            'type_of_delivery' => $request->input('type_of_delivery'),
            'date_of_delivery' => $request->input('date_of_delivery'),
            'place_of_delivery' => $request->input('place_of_delivery'),
            'birth_attendant' => $request->input('birth_attendant'),
            'birth_weight' => $request->input('birth_weight'),
            'hemorhage' => $request->input('hemorhage'),
            'alive' => $request->input('alive'),
            'healthy' => $request->input('healthy')
        ]);

        return response()->Json($maternalHealth);

    }

    public function getNextVisit(MaternalHealth $maternalHealth): JsonResponse
    {

        return response()->json($maternalHealth->nextVisit);

    }

    public function updateNextVisit(Request $request, MaternalHealth $maternalHealth): JsonResponse
    {
        $maternalHealth->nextVisit()
            ->update([
                'cell1' => $request->input('cell1'),
                'cell2' => $request->input('cell2'),
                'cell3' => $request->input('cell3'),
                'cell4' => $request->input('cell4'),
                'cell5' => $request->input('cell5'),
                'cell6' => $request->input('cell6'),
                'cell7' => $request->input('cell7'),
                'cell8' => $request->input('cell8')
            ]);
        
        return response()->json($maternalHealth);
    }

    public function search($keyword): JsonResponse
    {
        $response = MaternalHealth::where('name', 'like', "%{$keyword}%")
            ->paginate(10);

        return response()->json($response);
    }
}
