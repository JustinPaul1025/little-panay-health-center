<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\MaternalHealthDateOfVisit;

class MaternalHealthDateOfVisitController extends Controller
{
    public function update(Request $request, MaternalHealthDateOfVisit $maternalHealthDateOfVisit): JsonResponse
    {
        $maternalHealthDateOfVisit
            ->update([
                'cell1' => $request->input('cell1'),
                'cell2' => $request->input('cell2'),
                'cell3' => $request->input('cell3'),
                'cell4' => $request->input('cell4'),
                'cell5' => $request->input('cell5'),
                'cell6' => $request->input('cell6'),
                'cell7' => $request->input('cell7'),
                'cell8' => $request->input('cell8'),
                'cell9' => $request->input('cell9')
            ]);
        
        return response()->json($maternalHealthDateOfVisit);
    }
}
