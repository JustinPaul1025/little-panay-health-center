<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\Vaw;
use App\Models\Pelvic;
use App\Models\Physical;
use App\Models\Obsterical;
use Illuminate\Http\Request;
use App\Models\FamilyPlanning;
use App\Models\MedicalHistory;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class FamilyPlanningController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(FamilyPlanning::latest()->paginate(5));
    }

    public function store(Request $request): JsonResponse
    {
        $currentData = FamilyPlanning::create([
            'type_of_acceptor' => $request->input('type_of_acceptor'),
            'prev_method' => $request->input('prev_method'),
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'birthdate' => $request->input('birthdate'),
            'contact_no' => $request->input('contact_no'),
            'highest_education' => $request->input('highest_education'),
            'occupation' => $request->input('occupation'),
            'street' => $request->input('street'),
            'barangay' => $request->input('barangay'),
            'municipality' => $request->input('municipality'),
            'province' => $request->input('province'),
            'monthly_income' => $request->input('monthly_income'),
            'spouse_first_name' => $request->input('spouse_first_name'),
            'spouse_middle_name' => $request->input('spouse_middle_name'),
            'spouse_last_name' => $request->input('spouse_last_name'),
            'spouse_birthdate' => Carbon::parse($request->input('spouse_birthdate')),
            'spouse_highest_education' => $request->input('spouse_highest_education'),
            'spouse_occupation' => $request->input('spouse_occupation'),
            'number_lived_children' => $request->input('number_lived_children'),
            'plan_more_children' => $request->input('plan_more_children'),
            'reason' => $request->input('reason'),
            'method_accepted' => $request->input('method_accepted'),
            'acknowledgment' => $request->input('acknowledgment')
        ]);
        
        MedicalHistory::create([
            'family_planning_id' => $currentData->id,
            'epilepsy' => $request->input('epilepsy'),
            'dizziness' => $request->input('dizziness'),
            'blur_vision' => $request->input('blur_vision'),
            'conjuctiva' => $request->input('conjuctiva'),
            'thyroid' => $request->input('thyroid'),
            'chest_pain' => $request->input('chest_pain'),
            'fatigability' => $request->input('fatigability'),
            'axillary' => $request->input('axillary'),
            'nipple_discharge' => $request->input('nipple_discharge'),
            'systolic' => $request->input('systolic'),
            'diastolic' => $request->input('diastolic'),
            'cva_history' => $request->input('cva_history'),
            'mass_abdomen' => $request->input('mass_abdomen'),
            'gallbladder_disease' => $request->input('gallbladder_disease'),
            'liver_disease' => $request->input('liver_disease'),
            'mass_uterus' => $request->input('mass_uterus'),
            'vaginal_discharge' => $request->input('vaginal_discharge'),
            'intermenstrual_bleeding' => $request->input('intermenstrual_bleeding'),
            'postcoital_bleeding' => $request->input('postcoital_bleeding'),
            'varicosities' => $request->input('varicosities'),
            'severe_pain' => $request->input('severe_pain'),
            'yellowish_skin' => $request->input('yellowish_skin'),
            'smoking' => $request->input('smoking'),
            'allergies' => $request->input('allergies'),
            'drug_intake' => $request->input('drug_intake'),
            'bleeding_tendencies' => $request->input('bleeding_tendencies'),
            'anemia' => $request->input('anemia'),
            'diabetes' => $request->input('diabetes'),
            'hydatidiform' => $request->input('hydatidiform'),
            'ectopic_pregnancy' => $request->input('ectopic_pregnancy')
        ]);

        Obsterical::create([
            'family_planning_id' => $currentData->id,
            'full_term' => $request->input('full_term'),
            'premature' => $request->input('premature'),
            'abortions' => $request->input('abortions'),
            'living_children' => $request->input('living_children'),
            'date_last_delivery' => Carbon::parse($request->input('date_last_delivery')),
            'type_last_delivery' => $request->input('type_last_delivery'),
            'past_menstrual_periods' => $request->input('past_menstrual_periods'),
            'last_menstrual_periods' => $request->input('last_menstrual_periods'),
            'duration_menstrual_perios' => $request->input('duration_menstrual_perios')
        ]);
            
        Physical::create([
            'family_planning_id' => $currentData->id,
            'blood_pressure' => $request->input('blood_pressure'),
            'weight' => $request->input('weight'),
            'pulse_rate' => $request->input('pulse_rate'),
            'pale' => $request->input('pale'),
            'yellowish' => $request->input('yellowish'),
            'enlarge_thyroyd' => $request->input('enlarge_thyroyd'),
            'enlarge_lymph' => $request->input('enlarge_lymph'),
            'left_breast_mass' => $request->input('left_breast_mass'),
            'left_breast_nipple_discharge' => $request->input('left_breast_nipple_discharge'),
            'left_breast_orange_peel' => $request->input('left_breast_orange_peel'),
            'left_breast_enlarge_axillary' => $request->input('left_breast_enlarge_axillary'),
            'right_breast_mass' => $request->input('right_breast_mass'),
            'right_breast_nipple_discharge' => $request->input('right_breast_nipple_discharge'),
            'right_breast_orange_peel' => $request->input('right_breast_orange_peel'),
            'right_breast_enlarge_axillary' => $request->input('right_breast_enlarge_axillary'),
            'abnormal_heart' => $request->input('abnormal_heart'),
            'abnormal_breath' => $request->input('abnormal_breath'),
            'abdomen_enlarged_lever' => $request->input('abdomen_enlarged_lever'),
            'abdomen_mass' => $request->input('abdomen_mass'),
            'abdomen_tenderness' => $request->input('abdomen_tenderness'),
            'edema' => $request->input('edema'),
            'physical_varicosities' => $request->input('physical_varicosities')
        ]);

        Pelvic::create([
            'family_planning_id' => $currentData->id,
            'others' => $request->input('others'),
            'perinium_scars' => $request->input('perinium_scars'),
            'perinium_warts' => $request->input('perinium_warts'),
            'perinium_reddish' => $request->input('perinium_reddish'),
            'perinium_laceration' => $request->input('perinium_laceration'),
            'vagina_congested' => $request->input('vagina_congested'),
            'vagina_cyst' => $request->input('vagina_cyst'),
            'vagina_warts' => $request->input('vagina_warts'),
            'vagina_skene' => $request->input('vagina_skene'),
            'vagina_rectocele' => $request->input('vagina_rectocele'),
            'vagina_cystocele' => $request->input('vagina_cystocele'),
            'cervic_congested' => $request->input('cervic_congested'),
            'cervic_erosion' => $request->input('cervic_erosion'),
            'cervic_discharge' => $request->input('cervic_discharge'),
            'cervic_polyps' => $request->input('cervic_polyps'),
            'cervic_laceration' => $request->input('cervic_laceration'),
            'consistancy_firm' => $request->input('consistancy_firm'),
            'consistancy_soft' => $request->input('consistancy_soft'),
            'adnex_mass' => $request->input('adnex_mass'),
            'adnex_tenderness' => $request->input('adnex_tenderness'),
            'uterus_position' => $request->input('uterus_position'),
            'uterus_size' => $request->input('uterus_size'),
            'uterus_mass' => $request->input('uterus_mass'),
            'uterine_depth' => $request->input('uterine_depth')
        ]);

        Vaw::create([
            'family_planning_id' => $currentData->id,
            'vaw_history' => $request->input('vaw_history'),
            'unpleasant_relation' => $request->input('unpleasant_relation'),
            'partner_not_approved' => $request->input('partner_not_approved'),
            'partner_disagree' => $request->input('partner_disagree'),
            'reffered_to' => $request->input('reffered_to'),
            'other' => $request->input('other')
        ]);

        dd('Done here');

        return response()->json($currentData);
    }

    public function show(FamilyPlanning $familyPlanning): JsonResponse
    {
        return response()->Json($familyPlanning);
    }

    public function update(Request $request, FamilyPlanning $familyPlanning)
    {
        $familyPlanning->update([
            'type_of_acceptor' => $request->input('type_of_acceptor'),
            'prev_method' => $request->input('prev_method'),
            'first_name' => $request->input('first_name'),
            'middle_name' => $request->input('middle_name'),
            'last_name' => $request->input('last_name'),
            'birthdate' => $request->input('birthdate'),
            'contact_no' => $request->input('contact_no'),
            'highest_education' => $request->input('highest_education'),
            'occupation' => $request->input('occupation'),
            'street' => $request->input('street'),
            'barangay' => $request->input('barangay'),
            'municipality' => $request->input('municipality'),
            'province' => $request->input('province'),
            'monthly_income' => $request->input('monthly_income'),
            'spouse_first_name' => $request->input('spouse_first_name'),
            'spouse_middle_name' => $request->input('spouse_middle_name'),
            'spouse_last_name' => $request->input('spouse_last_name'),
            'spouse_birthdate' => Carbon::parse($request->input('spouse_birthdate')),
            'spouse_highest_education' => $request->input('spouse_highest_education'),
            'spouse_occupation' => $request->input('spouse_occupation'),
            'number_lived_children' => $request->input('number_lived_children'),
            'plan_more_children' => $request->input('plan_more_children'),
            'reason' => $request->input('reason'),
            'method_accepted' => $request->input('method_accepted'),
            'acknowledgment' => $request->input('acknowledgment')
        ]);

        return response()->json($familyPlanning);
    }

    public function destroy(FamilyPlanning $familyPlanning): JsonResponse
    {
        return response()->json($familyPlanning->delete());
    }

    public function search($keyword): JsonResponse
    {
        $response = FamilyPlanning::where('first_name', 'like', "%{$keyword}%")
            ->orWhere('last_name', 'like', "%{$keyword}%")
            ->paginate(10);

        return response()->json($response);
    }
}
