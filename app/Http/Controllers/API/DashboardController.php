<?php

namespace App\Http\Controllers\API;

use App\Models\ChildHealth;
use Illuminate\Http\Request;
use App\Models\FamilyProfile;
use App\Models\FamilyPlanning;
use App\Models\MaternalHealth;
use Illuminate\Http\JsonResponse;
use App\Models\IndividualTreatment;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function countDatas(): JsonResponse
    {
        $datas = array();
        $childhealth = ChildHealth::get();
        $familyPlanning = FamilyPlanning::get();
        $familyProfile = FamilyProfile::get();
        $individualTreatment = IndividualTreatment::get();
        $maternalHealth = MaternalHealth::get();
        array_push($datas, count($childhealth), count($familyPlanning), count($familyProfile), count($individualTreatment), count($maternalHealth));


        return response()->json($datas);
    }
}
