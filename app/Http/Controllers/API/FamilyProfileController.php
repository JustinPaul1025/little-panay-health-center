<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FamilyProfile;

use Illuminate\Http\JsonResponse;

class FamilyProfileController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(FamilyProfile::latest()->paginate(5));
    }

    public function store(Request $request): JsonResponse
    {
        $currentData = FamilyProfile::create([
            'name' => $request->input('name'),
            'address' => $request->input('address'),
        ]);

        return response()->json($currentData);
    }

    public function show(FamilyProfile $familyProfile): JsonResponse
    {
        return response()->json($familyProfile);
    }

    public function update(Request $request, FamilyProfile $familyProfile): JsonResponse
    {
        $familyProfile
          ->update([
                'name' => $request->input('name'),
                'address' => $request->input('address'),
            ]);
        
        return response()->json($familyProfile);
    }

    public function destroy(FamilyProfile $familyProfile): JsonResponse
    {
        return response()->json($familyProfile->delete());
    }

    public function search($keyword): JsonResponse
    {
        $response = FamilyProfile::where('name', 'like', "%{$keyword}%")->get();

        return response()->json($response);
    }
}
