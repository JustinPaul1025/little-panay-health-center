<?php

namespace App\Http\Controllers\API;

use Aloha\Twilio\Twilio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SmsController extends Controller
{
    public function sendSms(Request $request)
    {
        $number = $request->input('contact_no');
        $number = ltrim($number,"0");

        $account_id = 'AC990305fd4b7ba87e6b5ecc5ce7141b49';
        $auth_token = 'fcf97d02b86e4cd61348d7db04015427';
        $from_phone_number = '+12512741219';
        $twilio = new Twilio($account_id, $auth_token, $from_phone_number);

        $to_phone_number = '+63'.$number;
        $twilio->message($to_phone_number, $request->input('message'));

        return response()->json('success');
    }
}
