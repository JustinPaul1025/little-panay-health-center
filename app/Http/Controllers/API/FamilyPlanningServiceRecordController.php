<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\FamilyPlanning;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\FamilyPlanningServiceRecord;

class FamilyPlanningServiceRecordController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request): JsonResponse
    {
        $currentData = FamilyPlanningServiceRecord::create([
            'family_planning_id' => $request->input('family_planning_id'),
            'method_brand' => $request->input('method_brand'),
            'number_of_units' => $request->input('number_of_units'),
            'remarks' => $request->input('remarks'),
            'provider_name' => $request->input('provider_name'),
            'next_service' => Carbon::parse($request->input('next_service')),
        ]);

        return response()->json($currentData);
    }

    public function show($id): JsonResponse
    {
        $records = FamilyPlanning::find($id)->familyPlanningServiceRecords;
        return response()->Json($records);
    }

    public function update(Request $request, FamilyPlanningServiceRecord $familyPlanningServiceRecord): JsonResponse
    {
        $familyPlanningServiceRecord
            ->update([
                'family_planning_id' => $request->input('family_planning_id'),
                'method_brand' => $request->input('method_brand'),
                'number_of_units' => $request->input('number_of_units'),
                'remarks' => $request->input('remarks'),
                'provider_name' => $request->input('provider_name'),
                'next_service' => Carbon::parse($request->input('next_service')),
            ]);
        
        return response()->json($familyPlanningServiceRecord);
    }

    public function destroy(FamilyPlanningServiceRecord $familyPlanningServiceRecord): JsonResponse
    {
        return response()->json($familyPlanningServiceRecord->delete());
    }
}
