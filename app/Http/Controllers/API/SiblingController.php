<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\Sibling;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class SiblingController extends Controller
{
    public function store(Request $request): JsonResponse
    {
        $currentData = Sibling::create([
            'child_id' => $request->input('child_id'),
            'name' => $request->input('name'),
            'birthdate' => Carbon::parse($request['birthdate'])
        ]);

        return response()->json($currentData);
    }

    public function show($id): JsonResponse
    {
        return response()->json(Sibling::where('child_id', $id)->get());
    }

    public function update(Request $request, Sibling $sibling): JsonResponse
    {
        $sibling
            ->update([
                'name' => $request->input('name'),
                'birthdate' => Carbon::parse($request['birthdate'])
            ]);
        
        return response()->json($sibling);
    }

    public function destroy(Sibling $sibling): JsonResponse
    {
        return response()->json($sibling->delete());
    }
}
