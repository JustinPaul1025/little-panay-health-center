<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\IndividualTreatment;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class IndividualTreatmentController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json(IndividualTreatment::latest()->paginate(5));
    }

    public function store(Request $request): JsonResponse
    {
        $currentData = IndividualTreatment::create([
            'name' => $request->input('name'),
            'contact_no' => $request->input('contact_no'),
            'birthdate' => Carbon::parse($request->input('birthdate')),
            'gender' => $request->input('gender'),
            'civil_status' => $request->input('civil_status'),
            'address' => $request->input('address'),
            'occupation' => $request->input('occupation'),
            'philhealth' => $request->input('philhealth'),
            'spouse_name' => $request->input('spouse_name'),
            'spouse_birthdate' => Carbon::parse($request->input('spouse_birthdate')),
            'spouse_occupation' => $request->input('spouse_occupation'),
            'father_name' => $request->input('father_name'),
            'father_birthdate' => Carbon::parse($request->input('father_birthdate')),
            'father_occupation' => $request->input('father_occupation'),
            'mother_name' => $request->input('mother_name'),
            'mother_birthdate' => Carbon::parse($request->input('mother_birthdate')),
            'mother_occupation' => $request->input('mother_occupation')
        ]);

        return response()->json($currentData);
    }

    public function show(IndividualTreatment $individualTreatment): JsonResponse
    {
        return response()->json($individualTreatment);
    }

    public function update(Request $request, IndividualTreatment $individualTreatment): JsonResponse
    {
        $individualTreatment
        ->update([
            'name' => $request->input('name'),
            'contact_no' => $request->input('contact_no'),
            'birthdate' => Carbon::parse($request->input('birthdate')),
            'gender' => $request->input('gender'),
            'civil_status' => $request->input('civil_status'),
            'address' => $request->input('address'),
            'occupation' => $request->input('occupation'),
            'philhealth' => $request->input('philhealth'),
            'spouse_name' => $request->input('spouse_name'),
            'spouse_birthdate' => Carbon::parse($request->input('spouse_birthdate')),
            'spouse_occupation' => $request->input('spouse_occupation'),
            'father_name' => $request->input('father_name'),
            'father_birthdate' => Carbon::parse($request->input('father_birthdate')),
            'father_occupation' => $request->input('father_occupation'),
            'mother_name' => $request->input('mother_name'),
            'mother_birthdate' => Carbon::parse($request->input('mother_birthdate')),
            'mother_occupation' => $request->input('mother_occupation')
        ]);

        return response()->json($individualTreatment);
    }

    public function destroy(IndividualTreatment $individualTreatment): JsonResponse
    {
        return response()->json($individualTreatment->delete());
    }

    public function search($keyword): JsonResponse
    {
        $response = IndividualTreatment::where('name', 'like', "%{$keyword}%")->get();

        return response()->json($response);
    }
}
