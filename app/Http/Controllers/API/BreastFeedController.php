<?php

namespace App\Http\Controllers\API;

use App\Models\BreastFeed;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class BreastFeedController extends Controller
{
    public function show($id): JsonResponse
    {
        return response()->json(BreastFeed::where('child_id', $id)->first());
    }

    public function update(Request $request, BreastFeed $breastFeed): JsonResponse
    {
        $breastFeed
            ->update([
                'first_month' => $request->input('first_month'),
                'second_month' => $request->input('second_month'),
                'third_month' => $request->input('third_month'),
                'fourth_month' => $request->input('fourth_month'),
                'fifth_month' => $request->input('fifth_month'),
                'sixth_month' => $request->input('sixth_month'),
                'seventh_month' => $request->input('seventh_month'),
                'eight_month' => $request->input('eight_month')
            ]);

        return response()->json($breastFeed);
    }

}
