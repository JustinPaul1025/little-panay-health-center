<?php

namespace App\Http\Controllers\API;

use App\Models\ChildRecord;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class ChildRecordController extends Controller
{
    public function store(Request $request): JsonResponse
    {
        $currentData = ChildRecord::create([
            'child_id' => $request->input('child_id'),
            'weight_status' =>$request->input('weight_status'),
            'height_status' =>$request->input('height_status')
        ]);

        return response()->json($currentData);
    }

    public function show($id): JsonResponse
    {
        return response()->json(ChildRecord::where('child_id', $id)->get());
    }

    public function update(Request $request, ChildRecord $childRecord): JsonResponse
    {
        $childRecord
            ->update([
                'child_id' => $request->input('child_id'),
                'weight_status' =>$request->input('weight_status'),
                'height_status' =>$request->input('height_status')
            ]);

        return response()->json($childRecord);
    }

    public function destroy(ChildRecord $childRecord): JsonResponse
    {
        return response()->json($childRecord->delete());
    }
}
