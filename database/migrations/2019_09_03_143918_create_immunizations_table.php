<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImmunizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('immunizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('child_id');
            $table->date('bcg')->nullable();
            $table->date('hepa_b')->nullable();
            $table->date('nbs')->nullable();
            $table->date('nbh')->nullable();
            $table->date('rotavirus_first')->nullable();
            $table->date('rotavirus_second')->nullable();
            $table->date('rotavirus_third')->nullable();
            $table->date('pentavalent_first')->nullable();
            $table->date('pentavalent_second')->nullable();
            $table->date('pentavalent_third')->nullable();
            $table->date('pneumococcal_first')->nullable();
            $table->date('pneumococcal_second')->nullable();
            $table->date('pneumococcal_third')->nullable();
            $table->date('measles')->nullable();
            $table->date('mmr')->nullable();
            $table->date('vitamin_a')->nullable();
            $table->date('fic')->nullable();
            $table->timestamps();

            $table->foreign('child_id')->references('id')->on('child_healths')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('immunizations');
    }
}
