<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physicals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('family_planning_id');
            $table->string('blood_pressure');
            $table->float('weight');
            $table->float('pulse_rate');
            $table->boolean('pale')->nullable();
            $table->boolean('yellowish')->nullable();
            $table->boolean('enlarge_thyroyd')->nullable();
            $table->boolean('enlarge_lymph')->nullable();
            $table->boolean('left_breast_mass')->nullable();
            $table->boolean('left_breast_nipple_discharge')->nullable();
            $table->boolean('left_breast_orange_peel')->nullable();
            $table->boolean('left_breast_enlarge_axillary')->nullable();
            $table->boolean('right_breast_mass')->nullable();
            $table->boolean('right_breast_nipple_discharge')->nullable();
            $table->boolean('right_breast_orange_peel')->nullable();
            $table->boolean('right_breast_enlarge_axillary')->nullable();
            $table->boolean('abnormal_heart')->nullable();
            $table->boolean('abnormal_breath')->nullable();
            $table->boolean('abdomen_enlarged_lever')->nullable();
            $table->boolean('abdomen_mass')->nullable();
            $table->boolean('abdomen_tenderness')->nullable();
            $table->boolean('edema')->nullable();
            $table->boolean('physical_varicosities')->nullable();
            $table->timestamps();

            $table->foreign('family_planning_id')->references('id')->on('family_plannings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physicals');
    }
}
