<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaternalHealthFamilyHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maternal_health_family_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('maternal_health_id');
            $table->boolean('special_disease');
            $table->string('specific_special_disease')->nullable();
            $table->boolean('special_case');
            $table->string('specific_special_case')->nullable();
            $table->timestamps();

            $table->foreign('maternal_health_id')->references('id')->on('maternal_healths')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maternal_health_family_histories');
    }
}
