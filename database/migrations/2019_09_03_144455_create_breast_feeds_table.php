<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreastFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breast_feeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('child_id');
            $table->date('first_month')->nullable();
            $table->date('second_month')->nullable();
            $table->date('third_month')->nullable();
            $table->date('fourth_month')->nullable();
            $table->date('fifth_month')->nullable();
            $table->date('sixth_month')->nullable();
            $table->date('seventh_month')->nullable();
            $table->date('eight_month')->nullable();
            $table->timestamps();

            $table->foreign('child_id')->references('id')->on('child_healths')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breast_feeds');
    }
}
