<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaternalHealthLaborsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maternal_health_labors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('maternal_health_id');
            $table->boolean('immediate_breastfeeding')->nullable();
            $table->string('type_of_delivery')->nullable();
            $table->date('date_of_delivery')->nullable();
            $table->string('place_of_delivery')->nullable();
            $table->string('birth_attendant')->nullable();
            $table->float('birth_weight')->nullable();
            $table->boolean('hemorhage')->nullable();
            $table->boolean('alive')->nullable();
            $table->boolean('healthy')->nullable();
            $table->timestamps();

            $table->foreign('maternal_health_id')->references('id')->on('maternal_healths')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maternal_health_labors');
    }
}
