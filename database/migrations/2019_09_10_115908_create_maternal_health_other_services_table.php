<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaternalHealthOtherServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maternal_health_other_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('maternal_health_id');
            $table->integer('service_id');
            $table->string('action')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();

            $table->foreign('maternal_health_id')->references('id')->on('maternal_healths')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maternal_health_other_services');
    }
}
