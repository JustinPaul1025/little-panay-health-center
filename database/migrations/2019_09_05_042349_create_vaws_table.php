<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vaws', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('family_planning_id');
            $table->boolean('vaw_history')->nullable();
            $table->boolean('unpleasant_relation')->nullable();
            $table->boolean('partner_not_approved')->nullable();
            $table->boolean('partner_disagree')->nullable();
            $table->integer('reffered_to')->nullable();
            $table->string('other')->nullable();
            $table->timestamps();

            $table->foreign('family_planning_id')->references('id')->on('family_plannings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vaws');
    }
}
