<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelvicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelvics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('family_planning_id');
            $table->string('others')->nullable();
            $table->boolean('perinium_scars')->nullable();
            $table->boolean('perinium_warts')->nullable();
            $table->boolean('perinium_reddish')->nullable();
            $table->boolean('perinium_laceration')->nullable();
            $table->boolean('vagina_congested')->nullable();
            $table->boolean('vagina_cyst')->nullable();
            $table->boolean('vagina_warts')->nullable();
            $table->boolean('vagina_skene')->nullable();
            $table->boolean('vagina_rectocele')->nullable();
            $table->boolean('vagina_cystocele')->nullable();
            $table->boolean('cervic_congested')->nullable();
            $table->boolean('cervic_erosion')->nullable();
            $table->boolean('cervic_discharge')->nullable();
            $table->boolean('cervic_polyps')->nullable();
            $table->boolean('cervic_laceration')->nullable();
            $table->boolean('consistancy_firm')->nullable();
            $table->boolean('consistancy_soft')->nullable();
            $table->boolean('adnex_mass')->nullable();
            $table->boolean('adnex_tenderness')->nullable();
            $table->integer('uterus_position')->nullable();
            $table->integer('uterus_size')->nullable();
            $table->boolean('uterus_mass')->nullable();
            $table->float('uterine_depth')->nullable();
            $table->timestamps();

            $table->foreign('family_planning_id')->references('id')->on('family_plannings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelvics');
    }
}
