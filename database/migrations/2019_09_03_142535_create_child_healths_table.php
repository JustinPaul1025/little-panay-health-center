<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildHealthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_healths', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fsn_no');
            $table->string('nhts_no');
            $table->string('phic_no');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->date('birthdate');
            $table->string('contact_no');
            $table->string('address');
            $table->string('mother_name');
            $table->date('mother_birthdate');
            $table->string('mother_occupation');
            $table->string('father_name');
            $table->date('father_birthdate');
            $table->string('father_occupation');
            $table->date('tt_status');
            $table->string('birth_order');
            $table->boolean('gender');
            $table->date('ccr_register_date');
            $table->date('tcl_register_date');
            $table->string('marital_status');
            $table->string('delivery');
            $table->string('abnormalities');
            $table->float('birth_weight');
            $table->float('birth_length');
            $table->string('place_of_birth');
            $table->boolean('breast_feeding');
            $table->string('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_healths');
    }
}
