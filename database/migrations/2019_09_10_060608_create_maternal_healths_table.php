<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaternalHealthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maternal_healths', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('nhts_no');
            $table->integer('ips_no');
            $table->integer('phic_no');
            $table->string('name');
            $table->date('birthdate');
            $table->string('address');
            $table->string('contact_no');
            $table->string('civil_status');
            $table->string('place_of_birth');
            $table->string('occupation');
            $table->string('gravida');
            $table->string('para');
            $table->string('abortion');
            $table->date('date_last_delivery');
            $table->string('lmp');
            $table->string('edc');
            $table->string('husband_name');
            $table->date('husband_birthdate');
            $table->string('husband_occupation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maternal_healths');
    }
}
