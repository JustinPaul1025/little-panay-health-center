<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndividualTreatmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individual_treatments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('contact_no');
            $table->date('birthdate');
            $table->boolean('gender');
            $table->string('civil_status');
            $table->longText('address');
            $table->string('occupation')->nullable();
            $table->string('philhealth')->nullable();
            $table->string('spouse_name')->nullable();
            $table->date('spouse_birthdate')->nullable();
            $table->string('spouse_occupation')->nullable();
            $table->string('father_name')->nullable();
            $table->date('father_birthdate')->nullable();
            $table->string('father_occupation')->nullable();
            $table->string('mother_name')->nullable();
            $table->date('mother_birthdate')->nullable();
            $table->string('mother_occupation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individual_treatments');
    }
}
