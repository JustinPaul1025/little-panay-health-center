<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObstericalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obstericals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('family_planning_id');
            $table->integer('full_term')->default('0');
            $table->integer('premature')->default('0');
            $table->integer('abortions')->default('0');
            $table->integer('living_children')->default('0');
            $table->date('date_last_delivery')->nullable();
            $table->string('type_last_delivery')->nullable();
            $table->string('past_menstrual_periods')->nullable();
            $table->string('last_menstrual_periods')->nullable();
            $table->string('duration_menstrual_perios')->nullable();
            $table->timestamps();
            
            $table->foreign('family_planning_id')->references('id')->on('family_plannings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obstericals');
    }
}
