<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaternalHealthTrimestersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maternal_health_trimesters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('maternal_health_id');
            $table->integer('timester_type');
            $table->string('cell_1')->nullable();
            $table->string('cell_2')->nullable();
            $table->string('cell_3')->nullable();
            $table->string('cell_4')->nullable();
            $table->string('cell_5')->nullable();
            $table->string('cell_6')->nullable();
            $table->string('cell_7')->nullable();
            $table->string('cell_8')->nullable();
            $table->string('cell_9')->nullable();
            $table->timestamps();

            $table->foreign('maternal_health_id')->references('id')->on('maternal_healths')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maternal_health_trimesters');
    }
}
