<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaternalHealthDateNextVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maternal_health_date_next_visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('maternal_health_id');
            $table->date('cell1')->nullable();
            $table->date('cell2')->nullable();
            $table->date('cell3')->nullable();
            $table->date('cell4')->nullable();
            $table->date('cell5')->nullable();
            $table->date('cell6')->nullable();
            $table->date('cell7')->nullable();
            $table->date('cell8')->nullable();
            $table->timestamps();

            $table->foreign('maternal_health_id')->references('id')->on('maternal_healths')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maternal_health_date_next_visits');
    }
}
