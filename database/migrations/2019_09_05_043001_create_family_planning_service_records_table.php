<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyPlanningServiceRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_planning_service_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('family_planning_id');
            $table->string('method_brand');
            $table->float('number_of_units');
            $table->string('remarks');
            $table->string('provider_name');
            $table->date('next_service');
            $table->timestamps();

            $table->foreign('family_planning_id')->references('id')->on('family_plannings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_planning_service_records');
    }
}
