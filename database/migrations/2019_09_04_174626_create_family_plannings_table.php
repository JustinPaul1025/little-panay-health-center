<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyPlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_plannings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('type_of_acceptor');
            $table->string('prev_method')->nullable();
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->date('birthdate');
            $table->string('contact_no');
            $table->string('highest_education');
            $table->string('occupation');
            $table->string('street')->nullable();
            $table->string('barangay');
            $table->string('municipality');
            $table->string('province');
            $table->float('monthly_income');
            $table->string('spouse_first_name');
            $table->string('spouse_middle_name');
            $table->string('spouse_last_name');
            $table->date('spouse_birthdate');
            $table->string('spouse_highest_education');
            $table->string('spouse_occupation');
            $table->integer('number_lived_children');
            $table->boolean('plan_more_children');
            $table->string('reason');
            $table->integer('method_accepted');
            $table->string('acknowledgment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_plannings');
    }
}
