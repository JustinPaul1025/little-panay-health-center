<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('family_planning_id');
            $table->boolean('epilepsy')->nullable();
            $table->boolean('dizziness')->nullable();
            $table->boolean('blur_vision')->nullable();
            $table->boolean('conjuctiva')->nullable();
            $table->boolean('thyroid')->nullable();
            $table->boolean('chest_pain')->nullable();
            $table->boolean('fatigability')->nullable();
            $table->boolean('axillary')->nullable();
            $table->boolean('nipple_discharge')->nullable();
            $table->boolean('systolic')->nullable();
            $table->boolean('diastolic')->nullable();
            $table->boolean('cva_history')->nullable();
            $table->boolean('mass_abdomen')->nullable();
            $table->boolean('gallbladder_disease')->nullable();
            $table->boolean('liver_disease')->nullable();
            $table->boolean('mass_uterus')->nullable();
            $table->boolean('vaginal_discharge')->nullable();
            $table->boolean('intermenstrual_bleeding')->nullable();
            $table->boolean('postcoital_bleeding')->nullable();
            $table->boolean('varicosities')->nullable();
            $table->boolean('severe_pain')->nullable();
            $table->boolean('yellowish_skin')->nullable();
            $table->boolean('smoking')->nullable();
            $table->boolean('allergies')->nullable();
            $table->boolean('drug_intake')->nullable();
            $table->boolean('bleeding_tendencies')->nullable();
            $table->boolean('anemia')->nullable();
            $table->boolean('diabetes')->nullable();
            $table->boolean('hydatidiform')->nullable();
            $table->boolean('ectopic_pregnancy')->nullable();
            $table->timestamps();

            $table->foreign('family_planning_id')->references('id')->on('family_plannings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_histories');
    }
}
