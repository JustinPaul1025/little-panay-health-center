<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaternalHealthLaboratoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maternal_health_laboratories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('maternal_health_id')->nullable();
            $table->integer('laboratory_type');
            $table->date('date')->nullable();
            $table->string('result')->nullable();
            $table->timestamps();

            $table->foreign('maternal_health_id')->references('id')->on('maternal_healths')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maternal_health_laboratories');
    }
}
