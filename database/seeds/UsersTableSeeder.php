<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'username' => 'admin',
            'role' => 'admin',
            'first_name' => 'Adrian',
            'middle_name' => 'User',
            'last_name' => 'Administrator',
            'password' => bcrypt('123123'),
        ]);

        User::firstOrCreate([
            'username' => 'user',
            'role' => 'user',
            'first_name' => 'Adrian',
            'middle_name' => 'User',
            'last_name' => 'User',
            'password' => bcrypt('123123'),
        ]);
    }
}
