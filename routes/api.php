<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('dashboard', 'API\DashboardController@countDatas');

Route::post('sendSms', 'API\SmsController@sendSms');

Route::get('getProfile', 'API\UserController@profile');
Route::apiResources(['user' => 'API\UserController']);
Route::get('user/search/{keyword}', 'API\UserController@search');
Route::put('userChangePassword/{user}', 'API\UserController@changePassword');

Route::apiResources(['familyProfile' => 'API\FamilyProfileController']);
Route::apiResources(['familyProfileMember' => 'API\FamilyProfileMemberController']);
Route::get('familyProfile/search/{keyword}', 'API\FamilyProfileController@search');
Route::get('familyMembers/{id}', 'API\FamilyProfileMemberController@getFamilyMembers');

Route::apiResources(['individualTreatment' => 'API\IndividualTreatmentController']);
Route::apiResources(['treatmentRecord' => 'API\TreatmentRecordController']);
Route::get('individual/search/{keyword}', 'API\IndividualTreatmentController@search');
Route::get('treatmentRecords/{id}', 'API\TreatmentRecordController@getIndividualRecords');

Route::apiResources(['childHealth' => 'API\ChildHealthController']);
Route::get('childHealth/search/{keyword}', 'API\ChildHealthController@search');
Route::apiResources(['sibling' => 'API\SiblingController']);
Route::apiResources(['immunization' => 'API\ImmunizationController']);
Route::apiResources(['breastFeed' => 'API\BreastFeedController']);
Route::apiResources(['childRecord' => 'API\ChildRecordController']);

Route::apiResources(['familyPlanning' => 'API\FamilyPlanningController']);
Route::apiResources(['familyPlanningServiceRecord' => 'API\FamilyPlanningServiceRecordController']);
Route::get('familyPlanning/search/{keyword}', 'API\FamilyPlanningController@search');
Route::apiResources(['medicalHistory' => 'API\MedicalHistoryController']);
Route::apiResources(['obsterical' => 'API\ObstericalController']);
Route::apiResources(['physical' => 'API\PhysicalController']);
Route::apiResources(['vaw' => 'API\VawController']);
Route::apiResources(['pelvic' => 'API\PelvicController']);

Route::apiResources(['maternalHealth' => 'API\MaternalHealthController']);
Route::apiResources(['maternalHealthDateOfVisit' => 'API\MaternalHealthDateOfVisitController']);

Route::get('maternalHealth/obsterical/{maternalHealth}', 'API\MaternalHealthController@obsterical');
Route::get('maternalHealth/prev_pregnancy/{maternalHealth}', 'API\MaternalHealthController@prev_pregnancy');
Route::get('maternalHealth/family_history/{maternalHealth}', 'API\MaternalHealthController@family_history');
Route::get('maternalHealth/trimester/{maternalHealth}', 'API\MaternalHealthController@trimesters');
Route::get('maternalHealth/dateVisit/{maternalHealth}', 'API\MaternalHealthController@date_visit');
Route::get('maternalHealth/tetanus/{maternalHealth}', 'API\MaternalHealthController@getTetanus');
Route::get('maternalHealth/laboratory/{maternalHealth}', 'API\MaternalHealthController@getLaboratories');
Route::get('maternalHealth/services/{maternalHealth}', 'API\MaternalHealthController@getServices');
Route::get('maternalHealth/delivery/{maternalHealth}', 'API\MaternalHealthController@getDelivery');
Route::get('maternalHealth/nextVisit/{maternalHealth}', 'API\MaternalHealthController@getNextVisit');

Route::put('maternalHealth/updateTrimester/{maternalHealth}', 'API\MaternalHealthController@updateTrimester');
Route::put('maternalHealth/updateTetanus/{maternalHealth}', 'API\MaternalHealthController@updateTetanus');
Route::put('maternalHealth/updateLaboratories/{maternalHealth}', 'API\MaternalHealthController@updateLaboratories');
Route::put('maternalHealth/updateServices/{maternalHealth}', 'API\MaternalHealthController@updateServices');
Route::put('maternalHealth/updateDelivery/{maternalHealth}', 'API\MaternalHealthController@updateDelivery');
Route::put('maternalHealth/updateNextVisit/{maternalHealth}', 'API\MaternalHealthController@updateNextVisit');
Route::get('maternalHealth/search/{keyword}', 'API\MaternalHealthController@search');





